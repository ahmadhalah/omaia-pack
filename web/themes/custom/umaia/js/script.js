/**
 * @file
 * Behaviors for the hia theme.
 */

(function ($, _, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.language = {
    attach: function (context, settings) {
      $('.language-switcher-language-url').find('li[hreflang="' + drupalSettings.path.currentLanguage + '"]').hide();
    }
  };

  Drupal.behaviors.slick = {
    attach: function (context, settings) {
      var lang = drupalSettings.path.currentLanguage;

      $('.homepage-slider > .view-content').slick({
        dots: true,
        arrows: false,
        slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        rtl: (lang !== 'en')
      });

      $('.testimonials > .view-content').slick({
        dots: true,
        arrows: false,
        slidesToShow: 1,
        rtl: (lang !== 'en')
      });

      $('.block-content--type-clients .field--name-field-images').slick({
        arrows: false,
        dots: true,
        centerMode: true,
        slidesToShow: 5,
        draggable: false,
        rtl: (lang !== 'en'),
        responsive: [
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 3
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1
            }
          }
        ]
      });

      $('.homepage-case-studies .view-content .row').slick({
        dots: true,
        arrows: false,
        slidesToShow: 3,
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToScroll: 3,
        rtl: (lang !== 'en'),
        responsive: [
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1
            }
          }
        ]
      });
    }
  };

  Drupal.behaviors.colorbox = {
    attach: function (context, settings) {

      $('.block-content--type-video a').on('click', function (e) {
        e.preventDefault();
        $.colorbox({
          html: "<video autoplay controls><source src='https://www.omaiapack.com/sites/default/files/omaia-video.mp4'></video>",
          width: '100%',
          height: '100%',
          scrolling: false,
          fixed: true,
        });
      });
    }
  };

  Drupal.behaviors.masonry = {
    attach: function (context, settings) {
      var lang = drupalSettings.path.currentLanguage;
      $('.gallery .view-content').masonry({
        itemSelector: '.views-row',
        originLeft: (lang === 'en')
      });
    }
  };

  Drupal.behaviors.matchHeight = {
    attach: function () {
      $('.node--type-case-study.node--view-mode-teaser').matchHeight();
    }
  };

})(window.jQuery, window._, window.Drupal, window.drupalSettings);
